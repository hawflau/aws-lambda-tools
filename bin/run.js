#!/usr/bin/env node

const program = require('commander');
const LambdaFunction = require('../src/LambdaFunction');

program
    .version('0.0.1')
    .option('-E --env [env]', 'Specify env');

program
    .command('init')
    .description('Create a new lambda function project in cwd')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
        lambdaFunction
            .init()
            .then(result => {
                process.exit();
            });
    });

program
    .command('select-role')
    .description('Select an existing roles for the Lambda function')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .selectRole()
            .then(result => {
                process.exit();
            });
    });

program
    .command('view-role')
    .description('View the role for the Lambda function')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .viewRole()
            .then(result => {
                process.exit();
            });
    });

program
    .command('create-role')
    .description('Create a new execution role for the Lambda function')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .createRole()
            .then(result => {
                process.exit();
            });
    });

program
    .command('add-trigger')
    .description('Configure your Lambda function to respond to events from the selected trigger')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .addTrigger()
            .then(result => {
                process.exit();
            });
    });

program
    .command('deploy')
    .description('Deploy (create/update) the Lambda function')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .deploy()
            .then(result => {
                process.exit();
            });
    });

program
    .command('create-zip')
    .description('Create a zip file for the deployment package')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .createZip()
            .then(result => {
                process.exit();
            });
    });

program
    .command('invoke <eventPath>')
    .description('Invoke function')
    .action(eventPath => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .invoke(eventPath)
            .then(result => {
                process.exit();
            });
    });
    //.command('find-role <profile>', 'Find the IAM role for current project')
    //.command('create-role <profile>', 'Create AWS Lambda execution role for current project')
    //.command('deploy', 'Deploy function on AWS Lambda')

program
    .command('logs')
    .description('View logs')
    .action(() => {
        var lambdaFunction = new LambdaFunction(program.env);
    
        lambdaFunction
            .viewLogs()
            .then(result => {
                process.exit();
            });
    });

program.parse(process.argv);
