# aws-lambda-tools
Here includes a set of tools to automate deployments and management
of AWS Lambda functions. 

## Getting started
### Install
```
git clone git@github.com:hawflau/aws-lambda-tools.git
cd aws-lambda-tools
npm -g install
```

### Development
```
npm link
```

### Usage
```
  Usage: aws-lambda [options] [command]


  Commands:

    init                Create a new lambda function project in cwd
    select-role         Select an existing roles for the Lambda function
    view-role           View the role for the Lambda function
    create-role         Create a new execution role for the Lambda function
    add-trigger         Configure your Lambda function to respond to events from the selected trigger
    deploy              Deploy (create/update) the Lambda function
    create-zip          Create a zip file for the deployment package
    invoke <eventPath>  Invoke function
    logs                View logs

  Options:

    -h, --help      output usage information
    -V, --version   output the version number
    -E --env [env]  Specify env
```
