const AWS = require('aws-sdk');
const co = require('co');
const prompt = require('co-prompt');
const Table = require('cli-table');

const logger = require('./utils/logger');

const TRIGGER_TYPES = [
    'S3',
    //'API Gateway',
    //'AWS IoT',
    //'CloudWatch Events - Schedule',
    //'CloudWatch Logs',
    //'SNS'
];

const TRIGGER_CONFIG = {
    'S3': ['bucket', 'eventType', 'prefix', 'suffix'],
    //'API Gateway': [],
    //'AWS IoT': [],
    //'CloudWatch Events - Schedule': [],
    //'CloudWatch Logs': [],
    //'SNS': []
};

module.exports = function () {
    logger.primary('Adding trigger to Lambda function...');
    if (!this.config.triggers) {
        this.config.triggers = [];
    }

    return co(selectTriggerType)
        .then(choice => co.wrap(configureTrgger)(choice))
        .then(triggerConfig => {
            this.config.triggers.push(triggerConfig);
            this.save();
            return true;
        })
        .catch(err => {
            logger.info(err);
            return false;
        });
    
    function *selectTriggerType() {
        var table = new Table({
            head: ['No.', 'Trigger type']
        });

        TRIGGER_TYPES.forEach((type, i) => {
            table.push([i + 1, type]);
        });

        logger.default(table.toString());

        var choice = yield prompt(`Select a trigger type: (1 - ${TRIGGER_TYPES.length}) `);
    
        return choice - 1;
    }

    function *configureTrgger(choice) {
        var triggerType = TRIGGER_TYPES[choice]; 
        var triggerConfig = {
            type: triggerType,
            attrs: {}
        };

        for (var i=0; i < TRIGGER_CONFIG[triggerType].length; i++) {
            var attr = TRIGGER_CONFIG[triggerType][i];
            triggerConfig.attrs[attr] = yield prompt(`${attr}: `) || "";
        }
        return triggerConfig;
    } 
};

