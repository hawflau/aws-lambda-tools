const co = require('co');
const prompt = require('co-prompt');

const assumeRolePolicyTemplate = require('./assumeRolePolicy.json');
const executionPolicyBase = require('./executionPolicyBase.json');
const executionPolicyS3 = require('./executionPolicyS3.json');
const executionPolicyVPC = require('./executionPolicyVPC.json');

module.exports = function (config) {
    var policy = executionPolicyBase;

    if (config.function.vpcConfig) {
        policy.Statement.push(executionPolicyVPC);
    }

    //function *takeS3Buckets() {
    //    return yield 
    //}
};
