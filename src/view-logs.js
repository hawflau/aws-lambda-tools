const AWS = require('aws-sdk');
const moment = require('moment');

const logger = require('./utils/logger');
module.exports = function () {
    const config = this.config;

    const profile = config.credentials.profile;
    const logGroupName = `/aws/lambda/${config.function.functionName}`;
    const cloudwatchlogs = new AWS.CloudWatchLogs({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile }),
        region: config.region
    });
    logger.primary(`Using AWS profile ${profile}...`);

    logger.primary('Describing log group...');
    return getLogStreams(logGroupName)
        .then(logStreamName => {
            return getLogEvents(logGroupName, logStreamName);
        })
        .then(() => {
            //logger.primary(data);
            return true;
        })
        .catch(err => {
            logger.warning(err.message);
            return false;
        });

    function getLogStreams(logGroupName) {
        return cloudwatchlogs
            .describeLogStreams({
                logGroupName: logGroupName,
                descending: true,
                orderBy: 'LastEventTime'
            })
            .promise()
            .then(data => {
                const logStreams = data.logStreams;
                if (logStreams.length > 0) {
                    return logStreams[0].logStreamName;
                }
            
                throw new Error('No logstream found.');
            })
            .catch(err => {
                logger.warning(err);
                return false;
            });
    }

    function getLogEvents(logGroupName, logStreamName) {
        return cloudwatchlogs
            .getLogEvents({
                logGroupName: logGroupName,
                logStreamName: logStreamName,
                startFromHead: true
            })
            .promise()
            .then(data => {
                const events = data.events;

                const logMsg = events.map(event => {
                    var ts = moment(event.timestamp);
                    var msg = event.message;
                    return `[${ts.format('YYYY/MM/DD HH:mm:ss')}] ${msg}`;
                }).join('');

                logger.info(logMsg);

                return true;
            })
            .catch(err => {
                logger.warning(err);
                return false;
            });
    }
}
