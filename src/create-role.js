const co = require('co');
const prompt = require('co-prompt');
const AWS = require('aws-sdk');

const logger = require('./utils/logger');

const assumeRolePolicyTemplate = require('./policy-generator/assumeRolePolicy.json');
const executionPolicyBase = require('./policy-generator/executionPolicyBase.json');
const executionPolicyS3 = require('./policy-generator/executionPolicyS3.json');
const executionPolicyVPC = require('./policy-generator/executionPolicyVPC.json');


module.exports = function () {
    const ENV = this.ENV;
    var config = this.config;
    const profile = config.credentials.profile;
    const iam = new AWS.IAM({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile })
    });
    var roleArn;
    logger.primary(`Using AWS profile ${profile}...`);

    return co(function* () {
        var functionName = config.function.functionName;
        logger.primary(`Create AWS Lambda Execution Role for project ${functionName} (env: ${ENV})`);
        var defaultRoleName = ENV ? `${functionName}-execution-role-${ENV}` :
            `${functionName}-execution-role`;
        var roleName = yield prompt(`Role name: (${defaultRoleName}) `);

        return roleName || defaultRoleName;
    }).then(roleName => {
        var policy = executionPolicyBase;
        if (config.function.vpcConfig) {
            policy.Statement.push(executionPolicyVPC);
        }

        return {
            roleName: roleName,
            policy: policy
        };
    }).then(role => {
        return co(function* () {
            return yield prompt.confirm(`Add S3 permission? (y/N) `);
        }).then(answer => {
            if (answer) {
                return co(function* () {
                    return yield prompt(`bucket: (*) `);
                }).then(bucket => {
                    if (bucket !== '*' && bucket !== '') {
                        executionPolicyS3.Resource = `arn:aws:s3:::${bucket}/*`;
                    }
                
                    role.policy.Statement.push(executionPolicyS3);
                    return role;
                });
            }
        
            return role;
        });
    }).then(role => {
        logger.primary(`This will create a new role ${role.roleName} with inline policy:`); 
        logger.primary(JSON.stringify(role.policy, null, '    '));
        return co(function* () {
            return yield {
                role: role,
                confirm: prompt.confirm('Are you sure? ')
            };
        });
    }).then(result => {
        if (result.confirm) {
            logger.primary('Creating role...');
            return co(function* () {
                return yield {
                    iamP: iam
                        .createRole({
                            RoleName: result.role.roleName,
                            AssumeRolePolicyDocument: JSON.stringify(assumeRolePolicyTemplate)
                        })
                        .promise(),
                    role: result.role
                };
            });
        }

        logger.primary('cancelled.');
        throw new Error();
    }).then(result => {
        logger.primary('Creating inline policy...');
        roleArn = result.iamP.Role.Arn;
        return iam
            .putRolePolicy({
                RoleName: result.role.roleName,
                PolicyName: `${result.role.roleName}-policy`,
                PolicyDocument: JSON.stringify(result.role.policy)
            })
            .promise();
    }).then(result => {
        logger.primary('Execution role created!');
        config.function.role = roleArn;
        this.save();
        return true;
    }).catch(err => {
        return false;
    });
};
