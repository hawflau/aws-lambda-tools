const chalk = require('chalk');

class Logger {
    constructor () {}

    default(message) {
        if (typeof message === 'object') {
            message = JSON.stringify(message, null, '    ');
        }
        console.log(chalk.white('%s'), message);
    }

    primary(message) {
        if (typeof message === 'object') {
            message = JSON.stringify(message, null, '    ');
        }
        console.log(chalk.blue('%s'), message);
    }
    
    success(message) {
        if (typeof message === 'object') {
            message = JSON.stringify(message, null, '    ');
        }
        console.log(chalk.bold.green('%s'), message);
    }

    info(message) {
        if (typeof message === 'object') {
            message = JSON.stringify(message, null, '    ');
        }
        console.log(chalk.yellow('%s'), message);
    }

    warning(message) {
        if (typeof message === 'object') {
            message = JSON.stringify(message, null, '    ');
        }
        console.log(chalk.bgRed.white('%s'), message);
    }
}

module.exports = new Logger();
