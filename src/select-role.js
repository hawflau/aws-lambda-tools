const AWS = require('aws-sdk');
const co = require('co');
const prompt = require('co-prompt');
const Table = require('cli-table');

const logger = require('./utils/logger');
module.exports = function () {
    logger.primary(`Listing roles from AWS (profile: ${this.config.credentials.profile})`);

    const profile = this.config.credentials.profile;
    const iam = new AWS.IAM({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile })
    });
    var roles;

    return iam
        .listRoles()
        .promise()
        .then(data => {
            roles = data.Roles;

            if (roles && roles.length >= 1) {
                var table = new Table({
                    head: ['No.', 'Role name', 'Arn']
                });

                roles.forEach((role, i) => {
                    table.push([
                               i + 1,
                               role.RoleName,
                               role.Arn
                    ]);
                });

                logger.primary(table.toString());
                return co(function* () {
                    var choice = yield prompt(`Select role: (1 - ${roles.length}) `);
                    return choice;
                });
            } else {
                logger.primary('No role is available.');
                logger.primary('Please create a role for the Lambda function.');
                return false;
            }
        })
        .then(choice => {
            choice = +choice;
            if (Number.isInteger(choice) && choice >= 1 && choice <= roles.length) {
                var arn = roles[choice - 1].Arn;
                this.config.function.role = arn;
                this.save();
                return;
            }
            logger.primary('Invalid choice.');
            return false;
        })
        .catch(err => {
            logger.warning(err);
            return false;
        });
}
