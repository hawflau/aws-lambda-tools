const fs = require('fs');
const AWS = require('aws-sdk');

const logger = require('./utils/logger');
module.exports = function (eventPath) {
    const ENV = this.ENV;
    const config = this.config;
    const lambda = new AWS.Lambda({
        credentials: new AWS.SharedIniFileCredentials({
            profile: config.credentials.profile
        }),
        region: config.region
    });

    logger.primary(`Using AWS profile ${config.credentials.profile}...`);
    logger.primary(`Invoking Lambda Function (env: ${ENV})`);

    var payload = fs.readFileSync(eventPath);
    var params = {
        FunctionName: config.function.functionName,
        Payload: payload,
        InvocationType: 'RequestResponse',
        LogType: 'Tail'
    };

    return lambda
        .invoke(params)
        .promise()
        .then(data => {
            //logger.primary(data);
            var logResult = Buffer.from(data.LogResult, 'base64').toString('ascii');
            logger.info(logResult);
            //logger.primary(data.LogResult)
            return true;
        })
        .catch(err => {
            logger.warning(err);
            return false;
        });
}
