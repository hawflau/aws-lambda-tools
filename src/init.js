const AWS = require('aws-sdk');
const co = require('co');
const prompt = require('co-prompt');

const logger = require('./utils/logger');
module.exports = function () {
    var config = this.config = {};
    const ENV = this.ENV;
    return co(setAWSProfile)
        .then(() => co(setAWSRegion))
        .then(() => co(setFunctionParams))
        .then(() => co(confirmSetVPCConfig))
        .then(vpcConfig => {
            if (vpcConfig) {
                return co(setVPCConfig);
            }
            return;
        })
        .then(() => co(setEnvVar))
        .then(() => co(confirmFunc))
        .then(cfm => {
            if (cfm) {
                this.save();
                logger.primary('LambdaFunction is inited.');
                return true;
            }
            logger.primary('Operation cancelled.');
            logger.primary('LambdaFunction is not created.');
            return false;
        })
        .catch(err => {
            logger.warning(err);
            this.config = null;
            return false;
        });

    function *setAWSProfile() {
        var profile = yield prompt(`AWS profile (defined in ~/.aws/credentials): (default) `);

        config.credentials = {
            profile: profile || 'default'
        };

        return;
    }

    function *setAWSRegion() {
        var region = yield prompt(`AWS region: (ap-southeast-1) `);

        config.region = region || 'ap-southeast-1';

        return;
    }

    function *setFunctionParams() {
        const cwd = process.cwd();
        var tempFuncName = cwd.split('/').pop();
        if (ENV) {
            tempFuncName = `${tempFuncName}-${ENV}`
        }       
        var functionName = yield prompt(`function name: (${tempFuncName}) `);

        var description = yield prompt('description: ');
        var role = yield prompt('role: ');
        var handler = yield prompt('handler: ');
        var runtime = yield prompt('runtime: (nodejs4.3) ');
        var memorySize = yield prompt('memory size: (128) ');
        var timeout = yield prompt('timeout: (3) ');

        config.function = {
            functionName: functionName || tempFuncName,
            description: description || '',
            role: role || '',
            handler: handler || '',
            runtime: runtime || 'nodejs4.3',
            memorySize: memorySize || 128,
            timeout: timeout || 3
        };
        return;
    }

    function *confirmSetVPCConfig() {
        var vpcConfig = yield prompt.confirm('vpc? (y/N) ');
        return vpcConfig;
    }

    function *setVPCConfig() {
        var subnetIds = yield prompt('subnet id(s): (separated by ,) ');
        var securityGroupIds = yield prompt('security group id(s): (separated by ,) ');
        config.function.vpcConfig = {
            subnetIds: (subnetIds || '').split(','),
            securityGroupIds: (securityGroupIds || '').split(',')
        };
        return;
    }

    function *confirmFunc() {
        config.triggers = [];
        logger.primary(JSON.stringify(config, null, '    '));
        var cfm = yield prompt.confirm('Does it look good? (y/N) ');
        logger.primary(cfm);
        return cfm;
    }

    function *setEnvVar() {
        if (ENV) {
            if (config.function.runtime === 'nodejs4.3') {
                config.envVariables = [
                    `NODE_ENV=${ENV}`
                ];
            }
        }
        return;
    }
};


