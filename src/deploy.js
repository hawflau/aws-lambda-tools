const fs = require('fs');
const AWS = require('aws-sdk');
const _ = require('lodash');
const shortid = require('shortid');

const logger = require('./utils/logger');
module.exports = function () {
    const config = this.config;
    var functionArn;
    var zipLoc = this.ENV ? [
        this.ZIP_FILENAME,
        this.ENV,
        this.ZIP_EXT
    ].join('.') : [
        this.ZIP_FILENAME,
        this.ZIP_EXT
    ].join('.');
    zipLoc = `tmp/${zipLoc}`;

    const profile = config.credentials.profile;
    const lambda = new AWS.Lambda({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile }),
        region: config.region
    });
    logger.primary(`Using AWS profile ${profile}...`);

    return this.createZip()
        .then(result => {
            if (result) {
                return isFunctionExist();
            } else {
                throw new Error();
            }
        })
        .then(exists => {
            if (!exists) {
                return createFunction();
            }
            return updateFunction();
        })
        .then(result => {
            if (result) {
                return addTriggers();
            }

            logger.primary('Triggers are not configured.');
            return false;
        })
        .catch(err => {
            logger.warning(err);
            return false;
        });

    function isFunctionExist() {
        return lambda
            .getFunction({
                FunctionName: config.function.functionName
            })
            .promise()
            .then(data => {
                logger.primary('Lambda function exists.')
                return true;
            })
            .catch(err => {
                logger.warning(err.message);
                return false;
            });
    }

    function createFunction () {
        logger.primary('Creating Lambda Function on AWS...')
        var zipFileBuffer = fs.readFileSync(zipLoc);
        var params = {
            Code: {
                ZipFile: zipFileBuffer
            },
            FunctionName: config.function.functionName,
            Description: config.function.description,
            Role: config.function.role,
            Handler: config.function.handler,
            Runtime: config.function.runtime,
            MemorySize: config.function.memorySize,
            Timeout: config.function.timeout
        };

        if (config.function.vpcConfig) {
            params.VpcConfig = {
                SecurityGroupIds: config.function.vpcConfig.securityGroupIds,
                SubnetIds: config.function.vpcConfig.subnetIds
            };
        }

        return lambda
            .createFunction(params)
            .promise()
            .then(data => {
                logger.primary('Lambda function is created on AWS');
                logger.primary(data);
                functionArn = data.FunctionArn;
                return true;
            })
            .catch(err => {
                logger.warning(err.message);
                return false;
            });
    }

    function updateFunction () {
        logger.primary('Updating Lambda function code...');
        var zipFileBuffer = fs.readFileSync(zipLoc);
        var params = {
            FunctionName: config.function.functionName,
            Description: config.function.description,
            Role: config.function.role,
            Handler: config.function.handler,
            Runtime: config.function.runtime,
            MemorySize: config.function.memorySize,
            Timeout: config.function.timeout
        };

        if (config.function.vpcConfig) {
            params.VpcConfig = {
                SecurityGroupIds: config.function.vpcConfig.securityGroupIds,
                SubnetIds: config.function.vpcConfig.subnetIds
            };
        }

        return lambda
            .updateFunctionConfiguration(params)
            .promise()
            .then(data => {
                logger.primary('Lambda function config is updated.');
                logger.primary(data);
                functionArn = data.FunctionArn;
                return lambda
                    .updateFunctionCode({
                        FunctionName: config.function.functionName,
                        ZipFile: zipFileBuffer
                    }).promise();
            })
            .then(data => {
                logger.primary('Lambda function code is updated.');
                logger.primary(data);
                return true;
            })
            .catch(err => {
                logger.warning(err.message);
                return false;
            });
    }

    function addTriggers () {
        logger.primary('Adding permission for trigger sources to invoke Lambda function...');
        const triggers = config.triggers;
        const s3Triggers = triggers.filter(trigger => trigger.type === 'S3');

        return addS3Triggers(s3Triggers)
            .then(result => {
                logger.primary('All triggers have been configured.');
                return true;
            })
            .catch(err => {
                logger.warning(err);
                return false;
            });
    }

    function addS3Triggers(s3Triggers) {
        const s3 = new AWS.S3({
            credentials: new AWS.SharedIniFileCredentials({ profile: profile }),
            region: config.region
        });
        var buckets = _.uniq(s3Triggers.map(trigger => trigger.attrs.bucket));

        var permissionPromises = buckets.map(bucket => {
            var params = {
                Action: 'lambda:InvokeFunction',
                StatementId: `${config.function.functionName}-s3-${bucket}-${shortid.generate()}`,
                FunctionName: config.function.functionName,
                Principal: 's3.amazonaws.com',
                SourceArn: `arn:aws:s3:::${bucket}`
            };

            return lambda
                .addPermission(params)
                .promise()
                .then(data => {
                    logger.primary(data);
                    return true;
                })
                .catch(err => {
                    logger.warning(err);
                    return false;
                });
        });

        var bucketEventPromises = buckets.map(bucket => {
            var lambdaFunctionConfigs = s3Triggers
                .filter(trigger => {
                    return trigger.attrs.bucket === bucket;
                })
                .map(trigger => {
                    return {
                        Events: [trigger.attrs.eventType],
                        LambdaFunctionArn: functionArn,
                        Filter: {
                            Key: {
                                FilterRules: [{
                                    Name: 'prefix',
                                    Value: trigger.attrs.prefix
                                }, {
                                    Name: 'suffix',
                                    Value: trigger.attrs.suffix
                                }]
                            }
                        }
                    };
                });

            var params = {
                Bucket: bucket,
                NotificationConfiguration: {
                    LambdaFunctionConfigurations: lambdaFunctionConfigs
                }
            };

            return s3
                .putBucketNotificationConfiguration(params)
                .promise()
                .then(data => {
                    logger.primary(data);
                    return true;
                })
                .catch(err => {
                    logger.warning(err);
                    return false;
                });
        });

        var promises = permissionPromises.concat(bucketEventPromises);

        return Promise
            .all(promises)
            .then(result => {
                logger.primary(result);
                return true;
            })
            .catch(err => {
                logger.warning(err);
                return false;
            });
    }

};
