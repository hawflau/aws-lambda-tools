const fs = require('fs');
const path = require('path');
const JSZip = require('jszip');

const logger = require('./utils/logger');

module.exports = function () {
    logger.primary(`Creating zip package for deployment (env: ${this.ENV})...`);
    var currentPath = './';
    var zip = new JSZip();
    try {
        fs.readdirSync(path.join(currentPath, 'tmp'));
    } catch(err) {
        fs.mkdir(path.join(currentPath, 'tmp'));
    }
    var tempDir = path.join(currentPath, 'tmp');
    var CONFIG_FILENAME = this.CONFIG_FILENAME;
    var zipFileName = this.ENV ? [
        this.ZIP_FILENAME,
        this.ENV,
        this.ZIP_EXT
    ].join('.') : [this.ZIP_FILENAME, this.ZIP_EXT].join('.');

    if (this.ENV && this.config.envVariables) {
        zip.file('.env', this.config.envVariables.join('\n'));
    }

    var promise = new Promise((resolve, reject) => {
        zip = addDirToZip(zip, currentPath);
        zip
            .generateNodeStream({
                type: 'nodebuffer',
                streamFiles: true,
                compression: 'DEFLATE'
            })
            .pipe(fs.createWriteStream(path.join(tempDir, zipFileName)))
            .on('finish', () => {
                logger.primary('Deployment package generated.');
                resolve(true);
            })
            .on('error', err => {
                reject(err);
            });
    });

    return promise;

    function addFileToZip (zip, filePath) {
        //logger.primary(`Adding file ${filePath} to zip`);
        var fileContent = fs.readFileSync(filePath);
        zip.file(filePath, fileContent);
    }

    function addDirToZip (zip, dirPath) {
        //logger.primary(`Adding dir ${dirPath} to zip`);
        var currentPath = dirPath;

        var children = fs.readdirSync(currentPath);
        children.forEach(child => {
            if (!child.startsWith(CONFIG_FILENAME) && child !== zipFileName && child !== 'tmp') {
                var childPath = path.join(currentPath, child);
                if (fs.lstatSync(childPath).isFile()) {
                    addFileToZip(zip, childPath);
                }

                if (fs.lstatSync(childPath).isDirectory()) {
                    addDirToZip(zip, childPath);
                }
            }
        });

        return zip;
    }
};


