const AWS = require('aws-sdk');
const Table = require('cli-table');

const logger = require('./utils/logger');

module.exports = function () {
    const ENV = this.ENV;
    const config = this.config;
    const profile = config.credentials.profile;
    const iam = new AWS.IAM({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile })
    });
    logger.primary(`Using AWS profile ${profile}...`);

    const roleName = config.function.role.split('/').pop();

    return getRolePolicies(roleName)
        .then(result => {
            if (result) return true;
            return false;
        });

    function getRolePolicies(roleName) {
        logger.primary('Retrieving role policies...');

        return iam
            .listRolePolicies({
                RoleName: roleName,
            })
            .promise()
            .then(data => {
                var promises = data.PolicyNames.map(policyName => {
                    return iam
                        .getRolePolicy({
                            RoleName: roleName,
                            PolicyName: policyName
                        })
                        .promise();
                });
                
                return Promise.all(promises);
            })
            .then(result => {
                var table = new Table({
                    head: ['role', 'policy name', 'policy doc']
                });

                result.forEach(policy => {
                    var policyDoc = decodeURIComponent(policy.PolicyDocument.replace(/\+/g, '%20'));
                    policyDoc = JSON.parse(policyDoc);
                    policyDoc = JSON.stringify(policyDoc, null, '    ');
                    var row = [
                        policy.RoleName,
                        policy.PolicyName,
                        policyDoc
                    ];
                    table.push(row);
                });

                logger.primary(table.toString());
                return true;
            })
            .catch(err => {
                logger.warning(err);
                return false;
            });
    }
};
