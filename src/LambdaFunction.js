'use strict';

const path = require('path');
const fs = require('fs');
const co = require('co');
const prompt = require('co-prompt');
const AWS = require('aws-sdk');
const Table = require('cli-table');

const logger = require('./utils/logger');

const init = require('./init');
const selectRole = require('./select-role');
const createRole = require('./create-role');
const viewRole = require('./view-role');
const addTrigger = require('./add-trigger');
const createZip = require('./create-zip');
const deploy = require('./deploy');
const invoke = require('./invoke');
const viewLogs = require('./view-logs');

class LambdaFunction {
    constructor(env) {
        this.ENV = env;
        this.CONFIG_FILENAME = 'lambda-function-config';
        this.CONFIG_EXT = 'json';
        this.ZIP_FILENAME = 'deploy-package';
        this.ZIP_EXT = 'zip';
        this.config = null;
        this.load();
        this.init = init
        this.selectRole = this.isFunctionConfigured(selectRole);
        this.createRole = this.isFunctionConfigured(createRole);
        this.viewRole = this.isFunctionConfigured(viewRole);
        this.createZip = this.isFunctionConfigured(createZip);
        this.addTrigger = this.isFunctionConfigured(addTrigger);
        this.deploy = this.isFunctionConfigured(deploy);
        this.invoke = this.isFunctionConfigured(invoke);   
        this.viewLogs = this.isFunctionConfigured(viewLogs);
    }

    save() {
        logger.primary('Saving LambdaFunction config');
        var fileName = this.ENV ? [
            this.CONFIG_FILENAME,
            this.ENV,
            this.CONFIG_EXT
        ].join('.') : [
            this.CONFIG_FILENAME,
            this.CONFIG_EXT
        ].join('.');

        fs.writeFileSync(path.join(process.cwd(), fileName),
                         JSON.stringify(this.config, null, '    '));
        logger.primary(`${fileName} is saved.`);
        logger.primary(`You may edit ${fileName} to change your settings.`);
        return true;
    }

    load() {
        var fileName = this.ENV ? [
            this.CONFIG_FILENAME,
            this.ENV,
            this.CONFIG_EXT
        ].join('.') : [
            this.CONFIG_FILENAME,
            this.CONFIG_EXT
        ].join('.');

        try {
            this.config = require(path.join(process.cwd(), fileName));
            return true;
        } catch(err) {
            this.config = null;
            return false;
        }
    }

    isFunctionConfigured(func) {
        const config = this.config;
        return function () {
            if (!config) {
                logger.warning('lambdafunction is not configured in current working directory.');
                logger.warning(`please use 'am-lambda init' to create a project`);
                return new Promise((resolve, reject) => {
                    resolve(false);
                });
            }
            return func.call(this, ...arguments);
        };
    }
}

module.exports = LambdaFunction;
